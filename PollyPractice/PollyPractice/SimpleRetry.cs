﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollyPractice
{
    public class SimpleRetry
    {
        private int Trials;
        private int SuccesTrail;
        
        public SimpleRetry(int successTrail)
        {
            SuccesTrail = successTrail; 
        }

        public Response Try()
        {
            if (++Trials < SuccesTrail) { throw new Exception(); }
            return new Response();
        }
    }
}
