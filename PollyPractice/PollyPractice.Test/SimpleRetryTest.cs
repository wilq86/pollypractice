using FluentAssertions;
using Polly;

namespace PollyPractice.Test
{
    public class SimpleRetryTest
    {
        [Test]
        public void shouldBeSuccesful()
        {
            int retires = 0;
            SimpleRetry simpleRetry = new SimpleRetry(3);

            var policy = Policy<Response> 
                    .Handle<Exception>()
                    .Retry(3, onRetry: (exception, retryCount, context) =>
                    {
                        retires++;
                    });

            var result = policy.Execute(simpleRetry.Try);

            retires.Should().Be(2);
        }

        [Test]
        public void shouldBeFailed()
        {
            int retires = 0;
            SimpleRetry simpleRetry = new SimpleRetry(5);

            var policy = Policy<Response> 
                    .Handle<Exception>()
                    .Retry(3, onRetry: (exception, retryCount, context) =>
                    {
                        retires++;
                    });


            policy.Invoking(x => x.Execute(simpleRetry.Try))
               .Should()
               .Throw<Exception>();

            retires.Should().Be(3);
        }
    }
}