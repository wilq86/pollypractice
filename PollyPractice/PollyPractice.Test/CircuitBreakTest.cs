﻿using FluentAssertions;
using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollyPractice.Test
{
    [TestFixture]
    public class CircuitBreakTest
    {
        [Test]
        public void shouldFailingForSomeTime()
        {
            SimpleRetry simpleRetry = new SimpleRetry(3);

            var policy = Policy<Response>
                    .Handle<Exception>()
                    .CircuitBreaker(2, TimeSpan.FromSeconds(5));

            Stopwatch stopwatch = Stopwatch.StartNew();

            while (stopwatch.Elapsed < TimeSpan.FromSeconds(4))
            {
                policy
                    .Invoking(x => x.Execute(simpleRetry.Try))
                    .Should()
                    .Throw<Exception>();
            }

            Thread.Sleep(TimeSpan.FromSeconds(2));

            var result = policy.Execute(simpleRetry.Try);

            result.Should().NotBeNull();
        }
    }
}
