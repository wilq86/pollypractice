﻿using FluentAssertions;
using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollyPractice.Test
{
    [TestFixture]
    public class WaitAndRetryTest
    {
        [Test]
        public void shouldWait()
        {
            SimpleRetry simpleRetry = new SimpleRetry(4);

            var policy = Policy<Response>
                    .Handle<Exception>()
                    .WaitAndRetry(3,
                    (retires) => TimeSpan.FromSeconds(retires),
                        onRetry: (exception, retryCount, context) =>
                    {
                        
                    });

            Stopwatch stopwatch = Stopwatch.StartNew();
            var result = policy.Execute(simpleRetry.Try);
            stopwatch.Elapsed.Should()
                .BeGreaterThan(TimeSpan.FromSeconds(6))
                .And.BeLessThan(TimeSpan.FromSeconds(7));   
        }
    }
}
